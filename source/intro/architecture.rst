Architecture
===============

Data Hierarchy
^^^^^^^^^^^^^^

The Application is built around a few core contecepts.

* All objects are all connected back to "Brands" in one way or another.
* All vouchers are stored in Vaults.
* All Vouchers are awarded against a "Campaign"
* Campaigns define how and when vouchers can be awarded.

.. image:: images/data_hierarchy.png

