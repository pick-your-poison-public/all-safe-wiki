Terminology
===========

The specs, documentation and object model use a certain terminology that you should be aware of.

.. image:: images/terminology.png

Vendor
^^^^^^^^^^^^^^

A Vendor is the voucher vendor. (Used to Specify the "Type" of Voucher i.e "MTN Airtime")

Properties
**************

* **Name:** The name used to represent the vendor in the application UI (Friendly Name)
* **Normalized Name:** The key used to access this vendor via the redeption API. (always lowercase, and no spaces or characters)
* **Type:** Used to Specify if the Vendor is a Mobile Network or a Generic Voucher vendor.
* **Country:** The country the vendor is valid in.
* **Prefixes:** This is only available for "Mobile Network" type vendors, and allows valid prefixes to be set. (this allows for automatic vendor detection for mobile numbers)

Brand
^^^^^^^^^^^^^^

A Brand is an (Organisations/ Companies/ Brands) and is used to define owenership of Campaign, Vaults and Vouchers.

Properties
**************

* **Name:** The name used to represent the brand in the application UI (Friendly Name)
* **Contact Person:** The main contact person's name for the brand
* **Contact Number:** The main contact number for the brand
* **Contact Email Address:** The main contact email address for the brand

Vault
^^^^^^^^^^^^^^

A Vault is a Container used to hold Vouchers.

Properties
**************

* **Name:** The name used to represent the vault in the application UI (Friendly Name)
* **Description:** A description of what the vault is used for.
* **Brand:** The brand which has owenership of the Vault and the Vouchers contained in it.

Campaign
^^^^^^^^^^^^^^

A Vault is a Container used to hold Vouchers.

Properties
**************

* **Name:** The name used to represent the vault in the application UI (Friendly Name)
* **Description:** A description of what the vault is used for.
* **Brand:** The brand which has owenership of the Vault and the Vouchers contained in it.

Integration
^^^^^^^^^^^^^^

A Vault is a Container used to hold Vouchers.

Properties
**************

* **Name:** The name used to represent the vault in the application UI (Friendly Name)
* **Description:** A description of what the vault is used for.
* **Brand:** The brand which has owenership of the Vault and the Vouchers contained in it.