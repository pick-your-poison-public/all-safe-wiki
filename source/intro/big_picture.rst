The Big Picture
===============

What Is All-Safe?
^^^^^^^^^^^^^^^^^^^

All-Safe is a platform which facilitates the management and distribution of digital vouchers.

**Key Features**

* **Support for Multiple Voucher Types:** Vouchers can be loaded as either Airtime / Cellular Products Bound to a Network or Generic Vouchers.
* **High Availablity API:** asd

