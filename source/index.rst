.. AllSafe documentation master file, created by
   sphinx-quickstart on Thu Jun 14 23:28:33 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AllSafe's documentation!!
=====================================

In this Portal a wide number of topics are covered.

* **General System:** This covers general infomation about the platform, terminology used, etc.

* **User docs:** This covers infomation about platform workflows, e.g. Campaign Creation.

* **Developer docs:** This covers developer specific infomation.

    * Getting Started: Credentails, Endpoints, etc.

    * Quickstarts

    * Api Objects.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Introduction

   intro/big_picture
   intro/terminology
   intro/architecture

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Workflows
    
    workflows/vaults
    workflows/import_vouchers
    workflows/campaigns
    
.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Developers

    developers/home
    developers/redeption_api
    developers/quick_start
