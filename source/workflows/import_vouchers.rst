.. _refImportVouchers:
**********************
Voucher Imports
**********************

This section contains infomation about how vouchers are imported into the allsafe platform.


Campaign Statuses
###################

Campaign Status are automatically controlled by the system, and are updated when a campaign matches the criteria.

- **Pending:**
  This is the default status for a new campaign.

- **Live:**
  This indicates that a campaign is correctly configured.
  - If dates are specified and the current date must fall within this range.
  - Have atleast 1 vault linked.
  - Have atleast 1 integration with a status of [Active]

- **Completed:**
  Indicates that a campaign is over / closed.
  - If a campaign has an end date set and the current date must fall after this date.
  - Vouchers will no longer be awarded.

- **Archived:**
  Indicates that a campaign has completed sometime ago.
  - If the campaign end date is more than 30 days in the past.
  - The campaign no longer appears in the UI, unless included via filters.
  - Vouchers will no longer be awarded.


