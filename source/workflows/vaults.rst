***********
Vaults
***********

This section contains infomation about vaults, what they are used for and how to setup and configure them.

Vaults are 'containers' created by Brands to hold their digital vouchers.

.. image:: images/vaults_list.png

Vault Setup
###############



Vault Dashboard
#################


Advanced Actions
##################

Add Test Vouchers
********************
This allow you to create "sample vouchers" for testing purposes.

.. image:: images/vault_sample_vouchers_01.png

.. image:: images/vault_sample_vouchers_02.png


Transfer Vouchers
******************
This allows you to transfer between vaults as well as between brands.

.. image:: images/vault_transfer_vouchers.png

Export Vouchers
******************

This is the other way to get vouchers out of the platform.
When vouchers are exported they are marked off as used, so they cannot be reimported.

.. image:: images/vault_export_vouchers.png

Import Vouchers
******************

This allows a shortcut method to setup a :ref:`voucher import <refImportVouchers>`.