***************
Redeption API
***************
Allows 3rd party applications to perform the following actions.

It is a simple REST interface and secured via HTTP BASIC Authentication.

* Check voucher avaiablity of a campaign.
* Allocate a digital voucher.

Your Credentials can only access vouchers stored in vaults linked to your campaign.

Api End-Points and Definations.
################################

Please visit: `allsafe-api.poweredbytechsys.co.za/docs <https://allsafe-api.poweredbytechsys.co.za/docs>`_ :: 
To view the available API end-points and the model definations.

Api Test harness.
#####################

Visit `allsafe-api.poweredbytechsys.co.za <https://allsafe-api.poweredbytechsys.co.za/swagger>`_ :: 
to allow you to experiment with your integration without writing any code.

Obtaining Credentials
################################

Credentials consist of a username and password combination.

Credentials can be obtain from the campaign setup section of the management portal.

End-points.
################################

Availability
************************
Returns a list of available voucher types with the remaining number.

**Uri:** `allsafe-api.poweredbytechsys.co.za/availability <https://allsafe-api.poweredbytechsys.co.za/availability>`_ ::

**Method: ** HTTP-GET

.. _refApiStatuses:

Errors and status codes
################################

Status Codes
************************

Human interpretation of HTTP status codes.

======= =================== ========================================
Status  Defination          Result
======= =================== ========================================
200     Success             Action was completed successfully.
204     Success             Action was completed successfully.
401     Unauthorized        Unable to authenticate the request [#status1]_.
491     No Vault Linked     No vaults linked to your integrations campaign.
492     No vouchers         Either out of vouchers, nothing for the vendor / value combination
495     Validation Failed   Your request failed validation or was malformed [#status2]_.
496     Invalid Vendor      The supplied vendor is not known.
======= =================== ========================================

.. [#status1] **Authentication Errors:** Please see section on common authentication issues.

.. [#status2] **Validation:** Please see section on end-points and request objects.


Authentication Errors
--------------------------

A couple of common root causes for authorization issues.

* Invalid username and password.

* Integrations permission has been revocked.
    * Try checking the integration status in the management portal.

* Campaign is no longer 'live'.
    * Check the status of the campaign in the management portal.
    * Check the campaign start and end dates.

Common Errors
--------------------------

* **No vaults linked.**
    * Check to see that there are vaults linked to your campaign.

* **No vouchers.**
    * Check to see that there are available vouchers in the vaults linked to your campaign.
    * Check to see that there are available vouchers for the vendor and amount you are requesting.
    * Check there isn't a vendor / amount miss match [#err1]_.

.. [#err1] Does this vendor have vouchers for the requested denomination?