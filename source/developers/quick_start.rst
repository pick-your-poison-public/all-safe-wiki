Quick Start
===============
This is a quick Guide to get started quickly.

Dotnet Console App
++++++++++++++++++++++++++++++++

1. Install Nuget Package: "AllSafe.Client"

2. Register "IAllSafe" Interface

.. code-block:: c#
   :linenos:

    var section = this.Configuration.GetSection("yourSection");
    var cfg = section.Get<ApiConfiguration>();

    services.AddSingleton<IAllSafeConfig>(cfg);
    services.AddScoped<IAllSafe, AllSafeClient>();
