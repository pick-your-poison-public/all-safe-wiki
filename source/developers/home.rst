Developers Overview
====================

Access to the all-safe eco-system is avaiable to 3rd party application via REST api's.

For most use cases access to the redemption API will be sufficient.

Repository Statuses
+++++++++++++++++++++

.. |Badge| image:: https://gitlab.com/All-Safe/all-safe-backend/badges/master/build.svg

API Statuses
+++++++++++++
